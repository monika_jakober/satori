'use strict';

app.controller('LoginCarousel', 
	
	function LoginCarousel($scope){
	
		$('.btn').button();	
		$scope.myInterval = 8000;
		var slides = $scope.slides = [];

		  $scope.addSlide = function() {
		    var newWidth = 600 + slides.length;
		    slides.push({
		      	image: 'img/image_scroller1.png',
		      	header: 'Segmentation based on attitudes and shopping behavior',
		      	text: 'Unique segmentation of the total U.S. population',
		      	link: 'Learn More',
		      	linkUrl: '#',
		      	image2: 'img/image_scroller2.png'
		        	
		    },{
		    	image: 'img/image_scroller1.png',
		      	header: 'Segmentation based on attitudes and shopping behavior',
		      	text: 'Unique segmentation of the total U.S. population',
		      	link: 'Learn More',
		      	linkUrl: '#',
		      	image2: 'img/image_scroller2.png'
		    });
		  };
		  
		    $scope.addSlide();
		  
		  /*for (var i=0; i<4; i++) {
		    $scope.addSlide();
		  }*/
		
	}
	
	
);
