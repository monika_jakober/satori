'use strict';

app.controller('ModalDemoCtrl',
	function ModalDemoCtrl($scope, $modal, $log, $location, $http){
		
		
		$scope.go = function(url) {
			var item = this;
			var link = url;
			var newPath = '/reports/'+ url;
			
			//$location.path(newPath);
			var newS = $scope.getAttribute('href');
			console.log(newS);
  		};
		
		$scope.items = ['item1', 'item2', 'item3'];
		$scope.assortment = [
			{
				reportname: 'Basic Item Assortment',
				description: 'This is an item description for this report name ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Cross-Category Void',
				description: 'This is an item description for this report name number two ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Item Assortment',
				description: 'This is an item description for this report name ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Cross Channel',
				description: 'This is an item description for this report name number four ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Company Brand Item',
				description: 'This is an item description for this report name ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Item Pareto',
				description: 'This is an item description for this report name generated from Jaspersoft ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Retailer Report Suite',
				description: 'This is an item description for this report name this is another description ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Cross-Category - Brand Level',
				description: 'This is an item description for this report name ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Cross-Category - Market Only',
				description: 'This is an item description for this report name ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Cross-Category - Brand and Item Combined',
				description: 'This is an item description for this report name this is another descriptio ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Company Brand item  - with Competitive Info',
				description: 'This is an item description for this report name this is another descriptio ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Retail New Items',
				description: 'This is an item description for this report name generated from Jaspersoft ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Cat New Items',
				description: 'This is an item description for this report name  ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			},
			{
				reportname: 'Brand Void - need Dept/Cat/SubCat/BP',
				description: 'This is an item description for this report name this is another descriptio ',
				dateCreated: '11/24/2013',
				dateModified: '12/24/2013'
			}
			
		];
		$scope.radioModel = 'Assortment';
		
		$scope.open1D= function () {
		
		    var modalInstance = $modal.open({
		      templateUrl: 'views/1DStandardReports.html',
		      controller: ModalInstanceCtrl,
		      resolve: {
		        items: function () {
		          return $scope.assortment;
		        }
		      }
		    });
		
		    modalInstance.result.then(function (selectedItem) {
		      $scope.selected = selectedItem;
		    }, function () {
		      $log.info('Modal dismissed at: ' + new Date());
		    });
		};
		
		$scope.open1D2= function () {
		
		    var modalInstance = $modal.open({
		      templateUrl: 'views/1DStandardReports.html',
		      controller: ModalInstanceCtrl,
		      resolve: {
		        items: function () {
		          return $scope.assortment;
		        }
		      }
		    });
		
		    modalInstance.result.then(function (selectedItem) {
		      $scope.selected = selectedItem;
		    }, function () {
		      $log.info('Modal dismissed at: ' + new Date());
		    });
		};
		  
		  $("#datepickerS, #datepickerE").datepicker();
		  $('.btn').button();
	
		
	}
	
);

var ModalInstanceCtrl = function ($scope, $modalInstance, items) {

	  $scope.items = items;
	  $scope.selected = {
	    item: $scope.items[0]
	  };
	
	  $scope.ok = function () {
	    $modalInstance.close($scope.selected.item);
	  };
	
	  $scope.close = function () {
	    $modalInstance.dismiss('cancel');
	  };
	};