'use strict';

app.controller('CollapseDemoCtrl',

	function CollapseDemoCtrl($scope) {
  		$scope.isCollapsed = false;
  		$scope.go = function ( path ) {
  			$location.path( path );
  		};
	}
	
	
);
