'use strict';

app.controller('LoginController', 
	
	function LoginController($scope){
	
		$('.btn').button();	
		$scope.myInterval = 5000;
		var slides = $scope.slides = [];

		  $scope.addSlide = function() {
		    var newWidth = 600 + slides.length;
		    slides.push({
		    	
		      image: 'img/bankRankChart.png',
		      text: ['More','Extra','Lots of','Surplus'][slides.length % 4] +
		        	['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
		    });
		  };
		  for (var i=0; i<4; i++) {
		    $scope.addSlide();
		  }
		
	}
	
	
);
