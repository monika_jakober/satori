'use strict';

var app = angular.module('myApp', ['ngRoute', 'ngResource', 'ui.bootstrap', 'iFrame', 'overlaySwitch', 'topTenReports', 'highcharts-ng']);

app.config(function($routeProvider) {

	$routeProvider.when('/', {
		templateUrl : 'views/Login.html',
		controller : 'LoginController'
	}).when('/login', {
		templateUrl : 'views/Login.html',
		controller : 'LoginController'
	}).when('/reports', {
		templateUrl : 'views/Reports.html',
		controller : 'ReportsController'
	}).when('/launchpad', {
		templateUrl : 'views/Launchpad.html',
		controller : 'LaunchpadController'
	}).otherwise({
		redirectTo : '/'
	});

});

