'use strict';

angular.module('iFrame', []).directive('myFrame',

function () {
    return {
        restrict: 'E',
        require: '?ngModel',
        replace: true,
        transclude: true,
        template: '<iframe height="100%" width="100%" frameborder="0"></iframe>',
        link: function (scope, element, attrs) {
            element.attr('src', attrs.iframeSrc);
        }
    };
});