'use strict';

angular.module('topTenReports', []).directive('report', function($location) {
	return {
		restrict: 'C',
		scope: {
			url:'@'
		},
		link: function(scope, element, attrs) {
			var linkUrl = scope.url 
			
   			element.bind('click', function(){
   				$window.open('#/reports');
   				
   			});

   		}
            
	};
}); 